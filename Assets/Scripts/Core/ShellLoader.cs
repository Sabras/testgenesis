﻿using UnityEngine;

public class ShellLoader : MonoBehaviour
{
    [SerializeField] private GameObject shellPrefab;

    private static bool initialized;

    private void OnEnable()
    {
        if (!initialized)
        {
            Instantiate(shellPrefab);
            initialized = true;
        }
    }
}