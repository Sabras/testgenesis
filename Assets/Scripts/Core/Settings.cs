﻿using UnityEngine;

public class Settings : MonoBehaviour
{
    public static SettingsScriptable Instance;

    public SettingsScriptable SettingsScriptable;

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
        Instance = SettingsScriptable;
    }
}