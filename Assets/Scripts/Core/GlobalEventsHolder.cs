﻿using System;

public static class GlobalEventsHolder {
    
    public static Action<bool> GameOver;
    public static Action SceneStartsLoading;
    public static Action<bool> MusicSwitchEvent;
    public static Action RestartGameEvent;
}
