﻿using System.Collections.Generic;
using UnityEngine;

public class SettingsScriptable : ScriptableObject
{
    [Header("Global")]
    public AudioClip MusicClip;
    [Header("Player")]
    public string PlayerTag;
    public float TerrainMoveSpeed;
    public float PlayerMoveSmooth;
    public LayerMask InputLayerMask;
    public float PlayerBottomPositionBoundary;
    [Header("Game")]
    public List<ObstacleInfo> ObstacleInfos;
    public int MaxObstacleLineCapacity;
    public int ObstacleCorridorMinSize;
    public int ObstacleCorridorMaxSize;
    public float SpacingBetweenObstacles;
    public float ObstacleLinesStartDistance;
    public float ObstacleLinesQuantity;
    public float SpacingBetweenObstacleLines;
    public int MinCorridorOverlap;
    public ObstacleColor[] ObstacleColors;
}




