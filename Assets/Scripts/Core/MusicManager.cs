﻿using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private const string MUSIC_MUTE_KEY = "music";

    [SerializeField] private AudioSource audioSource;

    public static bool MusicMuted
    {
        get
        {
            var value = PlayerPrefs.GetInt(MUSIC_MUTE_KEY, 0);
            return value > 0;
        }
        set
        {
            var parameter = value ? 1 : 0;
            PlayerPrefs.SetInt(MUSIC_MUTE_KEY, parameter);
        }
    }

    private float musicLength;
    private float musicTimer;

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
        GlobalEventsHolder.MusicSwitchEvent += MusicSwitch;
        MusicSwitch(MusicMuted);
    }

    private void MusicSwitch(bool mute)
    {
        MusicMuted = mute;
        audioSource.mute = mute;
        PlayMusic();
    }

    private void Update()
    {
        if (musicTimer > musicLength)
        {
            PlayMusic();
        }

        musicTimer += Time.deltaTime;
    }

    private void PlayMusic()
    {
        audioSource.clip = Settings.Instance.MusicClip;
        musicLength = audioSource.clip.length;
        musicTimer = 0;
        audioSource.Play();
    }
}