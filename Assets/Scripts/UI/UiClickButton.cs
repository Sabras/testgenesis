﻿using UnityEngine;
using UnityEngine.UI;

public class UiClickButton : MonoBehaviour
{
    [SerializeField] private Button button;

    protected virtual void OnEnable()
    {
        button.onClick.AddListener(Click);
    }

    protected virtual void Click()
    {
    }

    private void Reset()
    {
        button = GetComponent<Button>();
    }
}