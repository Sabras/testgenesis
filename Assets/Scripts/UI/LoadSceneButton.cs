﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneButton : UiClickButton
{
    [Header("Load Scene")] public SceneType SceneType;

    protected override void Click()
    {
        GlobalEventsHolder.SceneStartsLoading?.Invoke();
        SceneManager.LoadScene(SceneType.ToString());
    }
}