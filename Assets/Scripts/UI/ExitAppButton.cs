﻿using UnityEngine;

public class ExitAppButton : UiClickButton
{
    protected override void Click()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
}