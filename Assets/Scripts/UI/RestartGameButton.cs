﻿public class RestartGameButton : UiClickButton
{
    protected override void Click()
    {
        GlobalEventsHolder.RestartGameEvent?.Invoke();
    }
}
