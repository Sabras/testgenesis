﻿using UnityEngine;
using UnityEngine.UI;

public class MusicSwitchButton : UiClickButton
{
    [SerializeField] private Sprite muteSprite;
    [SerializeField] private Sprite unMuteSprite;
    [SerializeField] private Image image;
    [SerializeField] private bool musicOnButton;

    protected override void OnEnable()
    {
        base.OnEnable();
        GlobalEventsHolder.MusicSwitchEvent += ChangeSprite;
        ChangeSprite(MusicManager.MusicMuted);
    }

    protected override void Click()
    {
        GlobalEventsHolder.MusicSwitchEvent.Invoke(musicOnButton);
    }

    private void ChangeSprite(bool mute)
    {
        image.sprite = mute ? muteSprite : unMuteSprite;
    }
}