﻿using UnityEngine;

public class GameCycleManager : MonoBehaviour
{
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject gameWinPanel;
    [SerializeField] private CollisionDetector winCollisionDetector;

    private void OnEnable()
    {
        GlobalEventsHolder.GameOver += GameOver;
        winCollisionDetector.CollisionEvent += WinLineCollision;
        GlobalEventsHolder.RestartGameEvent += ResetPanels;
    }

    private void GameOver(bool win)
    {
        if (win)
        {
            gameWinPanel.SetActive(true);
        }
        else
        {
            gameOverPanel.SetActive(true);
        }
    }

    private void ResetPanels()
    {
        gameOverPanel.SetActive(false);
        gameWinPanel.SetActive(false);
    }

    private void WinLineCollision(Collision collision)
    {
        var playerCollision = collision.collider.CompareTag(Settings.Instance.PlayerTag);
        if (playerCollision)
            GlobalEventsHolder.GameOver?.Invoke(true);
    }
}