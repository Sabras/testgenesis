﻿public class ObstacleLineController
{
    private ObstacleLineModel model;
    private ObstacleLineView view;
    
    public ObstacleLineController(ObstacleLineView view, ObstacleLineModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void Init(int corridorIndex)
    {
        model.Init(corridorIndex, view.TransformCached);
    }

    public int GetObstacleCorridorSize()
    {
        return model.CorridorSize;
    }   
    
    public int GetObstacleCorridorIndex()
    {
        return model.CorridorStartIndex;
    }

    public void DestroySelf()
    {
        model = null;
        view.Destroy();
    }
}
