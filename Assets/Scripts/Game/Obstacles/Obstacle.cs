﻿using System;
using UnityEngine;

[Serializable]
public class Obstacle : MonoBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private CollisionDetector collisionDetector;

    private bool isEvil;

    public float RealSize;

    public bool IsPulled { get; set; }

    private void OnEnable()
    {
        collisionDetector.CollisionEvent += Collision;
    }

    public void SetMaterial(bool evil)
    {
        meshRenderer.sharedMaterial = GetObstacleColorMaterial(evil);
        isEvil = evil;
    }

    public void Collision(Collision collision)
    {
        if (collision.collider == null)
        {
            return;
        }

        var playerCollision = collision.collider.CompareTag(Settings.Instance.PlayerTag);
        if (isEvil && playerCollision)
            GlobalEventsHolder.GameOver?.Invoke(false);
    }

    private Material GetObstacleColorMaterial(bool evil)
    {
        for (int i = 0; i < Settings.Instance.ObstacleColors.Length; i++)
        {
            if (Settings.Instance.ObstacleColors[i].Evil == evil)
                return Settings.Instance.ObstacleColors[i].Material;
        }

        return null;
    }
}