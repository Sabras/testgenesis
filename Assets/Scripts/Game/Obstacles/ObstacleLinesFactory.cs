﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObstacleLinesFactory : MonoBehaviour
{
    [SerializeField] private ObstacleLineView obstacleLineViewPrefab;
    [SerializeField] private Transform obstacleLinesSpawnParent;
    [SerializeField] private Transform gameWinLineTransform;

    private readonly List<ObstacleLineController> obstacleLineControllers = new List<ObstacleLineController>();

    void Start()
    {
        SpawnObstacleLines();
        GlobalEventsHolder.RestartGameEvent += ResetLineControllers;
    }

    private void SpawnObstacleLines()
    {
        var nextLinePosition = Settings.Instance.ObstacleLinesStartDistance;
        var corridorIndex = Random.Range(Settings.Instance.MinCorridorOverlap,
            Settings.Instance.MaxObstacleLineCapacity - Settings.Instance.MinCorridorOverlap);
        for (int i = 0; i < Settings.Instance.ObstacleLinesQuantity; i++)
        {
            var obstacleLineModel = new ObstacleLineModel();
            var obstacleLineView = Instantiate(obstacleLineViewPrefab, obstacleLinesSpawnParent); //todo cache transform
            obstacleLineView.transform.localPosition =
                new Vector3(obstacleLineView.transform.localPosition.x, obstacleLineView.transform.localPosition.y,
                    nextLinePosition);
            var obstacleLineController = new ObstacleLineController(obstacleLineView, obstacleLineModel);
            obstacleLineController.Init(corridorIndex);
            nextLinePosition += Settings.Instance.SpacingBetweenObstacleLines;
            obstacleLineControllers.Add(obstacleLineController);
        }

        var linePosition = gameWinLineTransform.localPosition;
        gameWinLineTransform.localPosition = new Vector3(linePosition.x, linePosition.y, nextLinePosition + 5f);
    }

    private void ResetLineControllers()
    {
        foreach (var obstacleLineController in obstacleLineControllers)
        {
            obstacleLineController.DestroySelf();
        }

        obstacleLineControllers.Clear();
        GC.Collect();
        SpawnObstacleLines();
    }
}

[Serializable]
public class ObstacleColor
{
    public Material Material;
    public bool Evil;
}