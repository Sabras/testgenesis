﻿using UnityEngine;

public class ObstacleLineModel
{
    public int CorridorStartIndex;
    public int CorridorSize;

    public void Init(int corridorIndex, Transform obstaclesParent)
    {
        SetCorridorIndexes(corridorIndex);
        GenerateObstacles(obstaclesParent);
    }

    private void GenerateObstacles(Transform obstaclesParent)
    {
        float lastObjectPosition = 0;

        for (int i = 0; i < Settings.Instance.MaxObstacleLineCapacity;)
        {
            var obstacleInfo = GetObstacle(Settings.Instance.MaxObstacleLineCapacity - i);
            var obstacle = Object.Instantiate(obstacleInfo.Prefab, obstaclesParent);
            obstacle.transform.parent = obstaclesParent;
            obstacle.transform.localPosition = new Vector3(lastObjectPosition +
                                                           Settings.Instance.SpacingBetweenObstacles, 0, 0);
            i += obstacleInfo.NodeSize;
            lastObjectPosition = (obstacle.RealSize) + obstacle.transform.localPosition.x;
            var corridorLastIndex = CorridorStartIndex + CorridorSize;
            if (i >= CorridorStartIndex && i <= corridorLastIndex)
            {
                obstacle.SetMaterial(false);
            }
            else
            {
                obstacle.SetMaterial(true);
            }
        }
    }

    private void SetCorridorIndexes(int corridorIndex)
    {
        var maxCapacity = Settings.Instance.MaxObstacleLineCapacity;
        CorridorSize = Random.Range(Settings.Instance.ObstacleCorridorMinSize,
            Settings.Instance.ObstacleCorridorMaxSize);
        var min = corridorIndex - CorridorSize + Settings.Instance.MinCorridorOverlap;
        if (min < 0) min = 0;
        var max = corridorIndex + CorridorSize - Settings.Instance.MinCorridorOverlap;
        if (max > maxCapacity) max = maxCapacity;
        CorridorStartIndex = Random.Range(min, max);
    }

    private ObstacleInfo GetObstacle(int maxSize)
    {
        var sizeObstacles = Settings.Instance.ObstacleInfos.FindAll(o => o.NodeSize <= maxSize);
        if (sizeObstacles.Count < 1)
        {
            Debug.LogErrorFormat("Can't find obstacle with max size {0}", maxSize);
            return null;
        }

        return sizeObstacles[Random.Range(0, sizeObstacles.Count)];
    }
}