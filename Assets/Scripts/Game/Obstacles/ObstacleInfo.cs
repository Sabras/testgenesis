﻿using System;

[Serializable]
public class ObstacleInfo
{
    public Obstacle Prefab;
    public int NodeSize;
}

