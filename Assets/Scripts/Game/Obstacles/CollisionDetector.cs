﻿using System;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    public Action<Collision> CollisionEvent;

    private void OnCollisionEnter(Collision other)
    {
        if (other == null)
        {
            return;
        }

        CollisionEvent?.Invoke(other);
    }
}