﻿using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private PlayerView playerView;
    [SerializeField] private Terrain terrain;

    private PlayerController playerController;

    private void OnEnable()
    {
        GlobalEventsHolder.RestartGameEvent += ResetPosition;
    }

    private void Start()
    {
        var playerModel = new PlayerModel();
        playerController = new PlayerController(playerView, playerModel);
        playerController.Init(terrain.terrainData.size.x / 2);
    }

    private void ResetPosition()
    {
        playerController.ResetPosition();
    }
}