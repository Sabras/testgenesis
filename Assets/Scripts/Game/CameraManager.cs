﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance;
    
    public Camera MainCamera;

    [SerializeField] private Transform transformCached;
    
    private Vector3 startPosition;
    private bool cameraMoving;

    private void OnEnable()
    {
        startPosition = transformCached.position;
        GlobalEventsHolder.RestartGameEvent += ResetCamera;
        GlobalEventsHolder.GameOver += win => cameraMoving = false;
        cameraMoving = true;
        Instance = this;
    }

    private void FixedUpdate()
    {
        if (!cameraMoving) return;
        transformCached.Translate(0, 0, Settings.Instance.TerrainMoveSpeed);
    }

    public void ResetCamera()
    {
        transformCached.position = startPosition;
        cameraMoving = true;
    }
}