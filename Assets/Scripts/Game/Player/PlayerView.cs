﻿using UnityEngine;

public class PlayerView : MonoBehaviour
{
    public Transform TransformCached;
    private Vector3 startPosition;
    
    private void OnEnable()
    {
        startPosition = TransformCached.position;
    }

    public void MovePlayer(Vector3 newPosition)
    {
        TransformCached.position = newPosition;
        TransformCached.Translate(0, 0, Settings.Instance.TerrainMoveSpeed);
    }

    public void ResetPosition()
    {
        TransformCached.position = startPosition;
    }
    
}