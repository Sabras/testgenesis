﻿using UnityEngine;

public class PlayerModel
{
    private Vector3 startRayHitPoint;
    private Vector3 startPosition;

    public Vector3 GetNewPosition(Vector3 currentPosition, float terrainBoundary)
    {
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = CameraManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, Settings.Instance.InputLayerMask))
            {
                startRayHitPoint = hit.point;
                startPosition = currentPosition;
            }
        }

        if (Input.GetMouseButton(0))
        {
            Ray ray = CameraManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, Settings.Instance.InputLayerMask))
            {
                var newPosition = hit.point - startRayHitPoint + startPosition;
          

                var newPos =  Vector3.Lerp(currentPosition,
                    new Vector3(newPosition.x, currentPosition.y, newPosition.z),
                    Time.deltaTime * Settings.Instance.PlayerMoveSmooth);
                
                //settings axis X boundary for player (left, right)
                var newPositionX = newPos.x;
                if ((newPositionX > 0 && newPositionX> terrainBoundary) ||
                    (newPositionX < 0 && newPositionX < -terrainBoundary))
                {
                    Debug.LogErrorFormat("restrain {0}", newPositionX);
                    newPos = new Vector3(currentPosition.x, currentPosition.y, newPos.z);
                }
                
                //settings axis Z boundary for player (bottom)
                if (newPos.z < Settings.Instance.PlayerBottomPositionBoundary)
                {
                    newPos.z = currentPosition.z;
                }
                return newPos;
            }
        }

        return currentPosition;
    }
}