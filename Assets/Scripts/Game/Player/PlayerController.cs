﻿public class PlayerController
{
    private PlayerView view;
    private PlayerModel model;
    private float terrainBoundary;
    private bool playerMoving;

    public PlayerController(PlayerView view, PlayerModel model)
    {
        this.view = view;
        this.model = model;
        UpdateUtil.FixedUpdateEvent += MoveUpdate;
        GlobalEventsHolder.GameOver += b => playerMoving = false;
        GlobalEventsHolder.RestartGameEvent += () => playerMoving = true;
        playerMoving = true;
    }

    public void Init(float terrainBoundary)
    {
        this.terrainBoundary = terrainBoundary;
    }

    public void ResetPosition()
    {
        view.ResetPosition();
    }

    private void MoveUpdate()
    {
        if (!playerMoving) return;
        var newPosition = model.GetNewPosition(view.TransformCached.position, terrainBoundary);
        view.MovePlayer(newPosition);
    }
}