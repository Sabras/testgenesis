﻿using System;
using UnityEngine;

public class UpdateUtil : MonoBehaviour
{
    public static Action UpdateEvent;
    public static Action FixedUpdateEvent;
    public static Action LateUpdateEvent;

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
        GlobalEventsHolder.SceneStartsLoading += ResetUpdateEvents;
    }

    private void ResetUpdateEvents()
    {
        UpdateEvent = null;
        FixedUpdateEvent = null;
        LateUpdateEvent = null;
    }

    private void Update()
    {
        UpdateEvent?.Invoke();
    }

    private void FixedUpdate()
    {
        FixedUpdateEvent?.Invoke();
    }

    private void LateUpdate()
    {
        LateUpdateEvent?.Invoke();
    }
}