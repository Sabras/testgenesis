﻿using UnityEngine;

public class DontDestroyGameObject : MonoBehaviour
{
    private void OnEnable()
    {
        DontDestroyOnLoad(this);
    }
}
